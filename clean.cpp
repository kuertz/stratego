#include <gtk/gtk.h>
#include "include/env.h"
#include "include/button.h"

void	destroy(GtkWidget* widg, t_env* env)
{
	(void)widg;
	(void)env;
	gtk_main_quit();
}
