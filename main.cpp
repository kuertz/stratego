#include <iostream>
#include "include/env.h"
#include "include/init_game.h"

int	main(int argc, char** argv)
{
	t_env	env;
	if (argc != 3 && argc != 1)
	{
		std::cerr << "./id_strat [Redfile Bluefile]" << std::endl;
		return (-1);
	}
	try
	{
		init_game(argc, argv, &env);
	}
	catch (int err)
	{
		std::cerr << "An error occured, exit prog" << std::endl;
	}
	gtk_main();
	return (0);
}
