NAME	=	id_strat
SRC	=	main.cpp		\
		init_game.cpp		\
		button.cpp		\
		button_event.cpp	\
		clean.cpp		\
		unit.cpp		\
		player.cpp
OBJ	=	$(SRC:.cpp=.o) 
CPP	=	g++
CPPFLAGS	+=	-W -Wall -Werror -g3 `pkg-config --cflags gtk+-2.0`
LDFLAGS +=	-L$(LIB) -lid `pkg-config --libs gtk+-2.0`

all:	$(NAME)

$(NAME):	$(OBJ)
	$(CPP) $(CFLAGS) -o $(NAME) $(OBJ) $(LDFLAGS)

clean:
	rm -f $(OBJ)

distclean:	clean
	rm -f $(NAME)

rebuild:	distclean all

.PHONY: all clean distclean rebuild
