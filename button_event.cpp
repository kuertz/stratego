#include <gtk/gtk.h>
#include <iostream>
#include "include/env.h"
#include "include/button.h"

void	click_button(GtkWidget* widget, button* tmp)
{
	(void)widget;
	if (tmp->getUnitTeam() == E_RED)
		std::cout << "RED" << std::endl;
	else if (tmp->getUnitTeam() == E_BLUE)
		std::cout << "BLUE" << std::endl;
}
