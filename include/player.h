#ifndef __PLAYER_H__
#define __PLAYER_H__

#include "enum.h"

class	Player
{
	private:
		int	nb_unit;
		char	tab[4][10];
		t_team	team;
	public:
		Player(void);
		Player(int, t_team);
		Player(Player const&);
		~Player(void);
		Player& operator=(Player const&);
		void	setCase(int, int, char);
		char	getCase(int, int);
		t_team	getTeam(void);
		void	printUnit(void);
};

#endif
