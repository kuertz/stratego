#ifndef __INIT_GAME_H__
#define __INIT_GAME_H__

typedef struct s_env	t_env;

int	init_game(int, char**, t_env*);

#endif
