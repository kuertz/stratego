#ifndef __ENUM_H__
#define __ENUM_H__

typedef enum	e_team
{
	E_RED = -42,
	E_BLUE = 42,
}		t_team;

typedef enum	e_rank
{
	E_FLAG = 0,
	E_BOMB = 1,
	E_SPY = 2,
	E_SCOUT = 4,
	E_MINER = 8,
	E_SERGEANT = 16,
	E_LIEUTENANT = 32,
	E_CAPTAIN = 64,
	E_MAJOR = 128,
	E_COLONEL = 256,
	E_GENERAL = 512,
	E_MARSHALL = 1024,
}		t_rank;


#endif
