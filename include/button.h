#ifndef __BUTTON_H__
#define __BUTTON_H__

#include <gtk/gtk.h>
#include "player.h"
#include "unit.h" 
#include "env.h"

class	button
{
	private:
		int		x;
		int		y;
		t_env*		env;
		GtkWidget*	but;
		unit		piece;
	public:
		button(int, int, t_env*, GtkWidget*, Player&);
		button(void);
		~button(void);
		button(button const&);
		button& operator=(button const&);
		GtkWidget*	getButton(void);
		t_env*		getEnv(void);
		t_team		getUnitTeam(void);
		int		getx(void);
		int		gety(void);
};

#endif
