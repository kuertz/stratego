#ifndef __UNIT_H__
#define __UNIT_H__

#include "enum.h"

class	unit
{
	private:
		t_rank	rank;
		t_team	team;
	public:
		unit(void);
		unit(t_rank, t_team);
		unit(unit const&);
		unit& operator=(unit const&);
		~unit(void);
		t_team	getTeam(void);
};

#endif
