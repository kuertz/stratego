#ifndef __ENV_H__
#define __ENV_H__

#include "player.h"
#include <gtk/gtk.h>
class button;

typedef struct	s_env
{
	Player		red;
	Player		blue;
	button*		but;
	GtkWidget*	window;
	GtkWidget*	table;
}		t_env;

#endif
