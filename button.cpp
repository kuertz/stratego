#include <iostream>
#include "include/button.h"
#include "include/button_event.h"

button::button(int x, int y, t_env* env, GtkWidget* widget, Player& player) : x(x), y(y), env(env), but(widget)
{
	if (player.getCase(x, y) == '1')
		this->piece = unit(E_MARSHALL, player.getTeam());
	if (player.getCase(x, y) == '2')
		this->piece = unit(E_GENERAL, player.getTeam());
	if (player.getCase(x, y) == '3')
		this->piece = unit(E_COLONEL, player.getTeam());
	if (player.getCase(x, y) == '4')
		this->piece = unit(E_MAJOR, player.getTeam());
	if (player.getCase(x, y) == '5')
		this->piece = unit(E_CAPTAIN, player.getTeam());
	if (player.getCase(x, y) == '6')
		this->piece = unit(E_LIEUTENANT, player.getTeam());
	if (player.getCase(x, y) == '7')
		this->piece = unit(E_SERGEANT, player.getTeam());
	if (player.getCase(x, y) == '8')
		this->piece = unit(E_MINER, player.getTeam());
	if (player.getCase(x, y) == '9')
		this->piece = unit(E_SCOUT, player.getTeam());
	if (player.getCase(x, y) == 'S')
		this->piece = unit(E_SPY, player.getTeam());
	if (player.getCase(x, y) == 'B')
		this->piece = unit(E_BOMB, player.getTeam());
	if (player.getCase(x, y) == 'F')
		this->piece = unit(E_FLAG, player.getTeam());
	g_signal_connect(this->but, "clicked", G_CALLBACK(click_button), &env->but[x * 10 + y]);
}

button::button(void)
{
}

button::~button(void)
{
}

button& button::operator=(button const& other)
{
	this->x = other.x;
	this->y = other.y;
	this->env = other.env;
	this->but = other.but;
	this->piece = other.piece;
	return (*this);
}

button::button(button const& other)
{
	this->x = other.x;
	this->y = other.y;
	this->env = other.env;
	this->but = other.but;
	this->piece = other.piece;
}

GtkWidget*	button::getButton(void)
{
	return (this->but);
}

t_env*		button::getEnv(void)
{
	return (env);
}

int		button::getx(void)
{
	return (this->x);
}

int		button::gety(void)
{
	return (this->y);
}

t_team	button::getUnitTeam(void)
{
	return (this->piece.getTeam());
}
