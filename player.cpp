#include <iostream>
#include "include/player.h"


Player::Player(void)
{
}

Player::Player(int i, t_team j)
{
	this->nb_unit = i;
	this->team = j;
}

Player::Player(Player const& other)
{
	this->nb_unit = other.nb_unit;
	this->team = other.team;
}

Player::~Player(void)
{
}

Player&	Player::operator=(Player const & other)
{
	this->nb_unit = other.nb_unit;
	this->team = other.team;
	return (*this);
}

void	Player::setCase(int x, int y, char value)
{
	this->tab[x][y] = value;
}

char	Player::getCase(int x, int y)
{
	return (this->tab[x][y]);
}

t_team	Player::getTeam(void)
{
	return (this->team);
}

void	Player::printUnit(void)
{
	int	i;
	int	j;

	i = 0;
	while (i < 4)
	{
		j = 0;
		while (j < 10)
		{
			std::cout << this->tab[i][j];
			j++;
		}
		std::cout << std::endl;
		i++;
	}
}
