#include "include/unit.h"

unit::unit(void)
{
}

unit::unit(t_rank rank, t_team team)
{
	this->rank = rank;
	this->team = team;
}

unit::~unit(void)
{
}

unit& unit::operator=(unit const& other)
{
	this->rank = other.rank;
	this->team = other.team;
	return (*this);
}

unit::unit(unit const& other)
{
	this->rank = other.rank;
	this->team = other.team;
}

t_team	unit::getTeam(void)
{
	return (this->team);
}
