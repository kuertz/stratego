#include <iostream>
#include <fstream>
#include "include/player.h"
#include "include/env.h"
#include "include/button.h"
#include "include/clean.h"

void	set_player(const char* value, Player& cur)
{
	int	i;
	int	j;

	i = 0;
	while (i < 4)
	{
		j = 0;
		while (j < 10)
		{
			cur.setCase(i, j, value[11 * i + j]);
			j++;
		}
		i++;
	}
}
 
int	read_file(const char* path, Player& cur)
{
	std::ifstream	myfile;
	char*		content = NULL;
	size_t		length;

	(void)cur;
	myfile.exceptions(std::ifstream::failbit);
	try
	{
		myfile.open(path);
		myfile.seekg(0, myfile.end);
		length = myfile.tellg();
		myfile.seekg(0, myfile.beg);
		if (length != 44)
		{
			throw("Input error check your file");
		}
		content = new char[length];
	}
	catch (std::exception& e)
	{
		throw(e.what());
	}
	myfile.read(content, length);
	set_player(content, cur);
	myfile.close();
	delete[] content;
	return (0);
}

void	set_window(int argc, char** argv, t_env* env)
{
	int		i;
	int		x;
	int		y;
	GtkWidget*	box;
	GdkColor	color;

	gtk_init(&argc, &argv);
	env->window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_window_set_default_size(GTK_WINDOW(env->window), 800, 800);
	g_signal_connect(env->window, "destroy", G_CALLBACK(destroy), env);
	g_signal_connect(env->window, "delete-event", G_CALLBACK(destroy), env);
	env->table = gtk_table_new(10, 10, FALSE);
	color.red = 0;
	color.blue = 65535;
	color.green = 0;
	i = 0;
	while (i < 100)
	{
		x = i / 10;
		y = i % 10;
		if (y == 4 || y == 5)
		{
			box = gtk_button_new_with_label(NULL);
			if (x == 2 || x == 3 || x == 6 || x == 7)
			{
				gtk_widget_modify_bg(box, GTK_STATE_NORMAL, &color);
			}
		}
		else
			box = gtk_button_new_with_label("XXX");
		if (y < 4)
			env->but[i] = button(x, y, env, box, env->blue);
		else if (y > 5)
			env->but[i] = button(x, y, env, box, env->red);
		gtk_table_attach_defaults(GTK_TABLE(env->table), box, x, x + 1, y, y + 1);
		i++;
	}
	gtk_container_add(GTK_CONTAINER(env->window), env->table);
	gtk_widget_show_all(env->window);
}

int	init_game(int argc, char** argv, t_env* env)
{
	env->red = Player(40, E_RED);
	env->blue = Player(40, E_BLUE);
	try
	{
		if (argc == 1)
		{
			read_file("default", env->red);
			read_file("default", env->blue);
		}
		else
		{
			read_file(argv[1], env->red);
			read_file(argv[2], env->blue);
		}
		env->but = new button[100];
		set_window(argc, argv, env);
	}
	catch (std::exception& e)
	{
		std::cout << e.what() << std::endl;
	}
	catch (const char* str)
	{
		std::cerr << str << std::endl;
		throw (1);
	}
	return (0);
}
